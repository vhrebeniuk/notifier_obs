import psycopg2

class ObservedError(Exception):

    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super(ObservedError, self).__init__(message)

        self.message = message

    def __str__(self):
        return self.message

class Observer:
    _observers = []

    def __init__(self):
        self._observers.append(self)
        self._observables = {}

    def observe(self, event_name, callback):
        self._observables[event_name] = callback


class Event:

    def __init__(self, name, data):
        self.name = name
        self.data = data

    def fire(self):
        for observer in Observer._observers:
            if self.name in observer._observables:
                observer._observables[self.name](self.data)


class Observer_db(Observer):

    def __init__(self, port='5432', user='postgres', host="192.168.0.13",
                 password='aez123', db_name="log_db", project='tst_project'):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.db_name = db_name
        self.project = project
        Observer.__init__(self) # Observer's init needs to be called

    def __enter__(self):
        try:
            self.conn = psycopg2.connect(
                host=self.host,
                port=self.port,
                dbname=self.db_name,
                user=self.user,
                password=self.password
                                    )
            self.cursor = self.conn.cursor()
            return self.cursor
        except Exception as e:
            self.rize_notification(e)
            self.__exit__(e)

    def __exit__(self, *args, **kwargs):
        self.conn.close()

    def check_db(self):
        self.rize_notification(error="beda")

    def db_full_name(self):
        return self.project + '_' + self.host

    def rize_notification(self, error):
        raise ObservedError(error)


if __name__=='__main__':
    db = Observer_db()
    with db:
        db.observe(db.db_full_name(), db.check_db())

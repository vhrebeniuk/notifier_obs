"""
Functions for observer
"""

"""
Напиши функцию, которая будет подключаться к Postgresql базе и
делать запрос/ Результат запроса обект datetime.
Нужно провести анализ времени, если это день, то разница между временим и текущим
должна быть не больше 30 минут. Если ночь то последняя запись должна быть в 11 вечера минимум.
Ночь это с 11 до 7 утра.


postgresql:
name db - log_db
login - postgres
password - aez123
host - 192.168.0.13
port - 5432
"""
from datetime import datetime, timedelta

import psycopg2

with psycopg2.connect(database='log_db', user='postgres', password='aez123', host='192.168.0.13', port='5432') as conn:
#     conn.execute("SELECT timestamp FROM log")

    is_day = True
    now = datetime.today()
    day = datetime(2018,7,21,13,0,0)
    night = datetime(2018,7,21,2,15,0)
    chek_date = now.hour
    if chek_date >= 7 and chek_date <= 23:
        is_day = True
    elif chek_date >= 0 and chek_date < 7 or chek_date > 23 and chek_date < 24:
        is_day = False

    delta_time = now - night

    if is_day:
        delta_time = delta_time.total_seconds() / 60
        delta_time = round(float(delta_time), 2)

        if delta_time < 30:

            print('day ok ',delta_time)
        else:
            print('day not ok ',delta_time)
    else:
        night_time = datetime(now.year,now.month,now.day - 1,23,0,0)
        div_night_time = now - night_time
        print(div_night_time)
        if div_night_time >= delta_time:
            print('night ok ', delta_time)
        else:
            print('night not ok', delta_time)






